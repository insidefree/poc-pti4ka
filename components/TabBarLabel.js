import React from 'react'
import Colors from '../constants/Colors'
import {Text} from 'react-native'

export default function MyTabBarLabel (props) {
    return (
        <Text 
        style={{
            textAlign: 'center', 
            fontSize: 10, color: props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}}>
            {props.name}
        </Text>
    )
  }