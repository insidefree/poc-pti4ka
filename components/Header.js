import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    Image
} from 'react-native'

const Header = ({ title }) => {
    return (
        <View style={styles.headerContainer}>
            <Text style={styles.textHeaderContainer}>
                {title}
            </Text>
        </View>
    )
}

export { Header }

const styles = StyleSheet.create({
    headerContainer: {
        height: 75,
        justifyContent: 'center',
        backgroundColor: '#de3b43',
        paddingLeft: 25,
        paddingTop: 30,
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    textHeaderContainer: {
        color: '#fff',
        fontSize: 28,
        fontFamily: 'AvenirNext-DemiBold'
    }
})
