import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    Image
} from 'react-native'
import dimensions from '../constants/Layout'

const CoffeeCard = (props) => {
    const { item, item: { imageLink } } = props
    const { container, cover, h1, desc } = styles
    return (
        <View style={container}>
            <Image
                style={cover}
                source={imageLink}
            />
            <Text style={h1}>{item.title}</Text>
            <Text style={desc}>
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // background   Color: 'gold',
        height: 430,
        width: dimensions.window.width * 0.8,
        // shadowColor: '#000',
        // shadowRadius: 8,
        // shadowOffset: {
        //     width: 0,
        //     height: 5
        // },
        // shadowOpacity: 0.4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cover: {
        height: dimensions.window.width * 0.7,
        width: dimensions.window.width * 0.7,
        borderRadius: 20
    },
    h1: {
        fontFamily: 'AvenirNext-DemiBold',
        fontSize: 28,
        textAlign: 'center',
        marginTop: 10
    },
    desc: {
        fontFamily: 'AvenirNext-DemiBold',
        fontSize: 14,
        color:'#6b6d6c',
        textAlign: 'center',
        marginTop: 10
    }
})

export { CoffeeCard }
