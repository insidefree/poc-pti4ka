import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Header } from '../components'

export class ContactsScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        const { container, message, email } = styles
        return (
            <View style={{flex: 1}}>
            <Header title={'Contacts'} />
                <View style={container}>
                    <Text style={message}>
                        ping me by:
                </Text>
                    <Text style={email}>
                        pti4ka@pti4ka.coffee
                </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#de3b43'
    },
    message: {
        fontFamily: 'AvenirNext-DemiBold',
        fontSize: 28,
        color: '#fff',
        marginBottom: 30
    },
    email: {
        fontFamily: 'AvenirNext-DemiBold',
        color: '#fff',
        fontSize: 36
    }
})