import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import MyTabBarLabel from '../components/TabBarLabel'
// import HomeScreen from '../screens/HomeScreen';
// import LinksScreen from '../screens/LinksScreen';
// import SettingsScreen from '../screens/SettingsScreen';

import { HomeScreen, CoffeTypesScreen, ContactsScreen } from '../screens'

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: ({ focused }) => (
    <MyTabBarLabel
      focused={focused}
      name='WELCOME'
    />
  ),
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-heart${focused ? '' : '-outline'}`
          : 'md-heart'
      }
    />
  ),
  tabBarOptions: {
    style: {
      height: 45,
      marginBottom: 5
    },
  }
}

const CoffeTypesScreenStack = createStackNavigator({
  Coffee: CoffeTypesScreen,
});

CoffeTypesScreenStack.navigationOptions = {
  tabBarLabel: ({ focused }) => (
    <MyTabBarLabel
      focused={focused}
      name='COFFEE'
    />
  ),
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-analytics${focused ? '' : '-outline'}` : 'ion-md-analytics'}
    />
  ),
  tabBarOptions: {
    style: {
      height: 45,
      marginBottom: 5
    },
  }
};

const ContactsStack = createStackNavigator({
  Contacts: ContactsScreen,
});

ContactsStack.navigationOptions = {
  tabBarLabel: ({ focused }) => (
    <MyTabBarLabel
      focused={focused}
      name='CONTACTS'
    />
  ),
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-game-controller-b${focused ? '' : '-outline'}` : 'md-game-controller-b'}
    />
  ),
  tabBarOptions: {
    style: {
      height: 45,
      marginBottom: 5
    },
  }
};

export default createBottomTabNavigator({
  HomeStack,
  CoffeTypesScreenStack,
  ContactsStack
});
